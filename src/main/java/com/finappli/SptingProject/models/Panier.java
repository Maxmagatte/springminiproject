package com.finappli.SptingProject.models;


import lombok.Data;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

@Data
@Entity
public class Panier {

	 @ManyToMany(mappedBy = "paniers")
  private List <Product> products = new ArrayList<Product>();
  private @Id @GeneratedValue Long id;
  private String desccription;

  Panier() {}

public List<Product> getProducts() {
	return products;
}

public void setProducts(List<Product> products) {
	this.products = products;
}

public Long getId() {
	return id;
}

public void setId(Long id) {
	this.id = id;
}

public String getDesccription() {
	return desccription;
}

public void setDesccription(String desccription) {
	this.desccription = desccription;
}

  
}