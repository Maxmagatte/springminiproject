package com.finappli.SptingProject.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.finappli.SptingProject.models.Panier;
import com.finappli.SptingProject.models.Product;


public interface PanierRepository extends JpaRepository<Panier, Long> {
		
}