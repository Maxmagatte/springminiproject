package com.finappli.SptingProject.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.finappli.SptingProject.models.Product;


public interface   ProductRepository extends JpaRepository<Product, Long> {



}
