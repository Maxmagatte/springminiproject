package com.finappli.SptingProject.controller;

import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.finappli.SptingProject.dao.ProductRepository;
import com.finappli.SptingProject.models.Product;
import com.finappli.SptingProject.utils.ProductNotFoundException;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "http://localhost:8081")
public class ProductController {

	private final ProductRepository repository;

	ProductController(ProductRepository repository) {
		this.repository = repository;
	}

	// Recuperer la liste des produit

	@GetMapping("/products")
	List<Product> all() {
		return repository.findAll();
	}

	// ajouter un produit

	@PostMapping("/products")
	Product newProduct(@RequestBody Product newProduct) {
		return repository.save(newProduct);
	}

	// Recuperer les informations d un produit

	@GetMapping("/products/{id}")
	Product findOne(@PathVariable Long id) {

		return repository.findById(id).orElseThrow(() -> new ProductNotFoundException(id));
	}

// Mettre a jour un produit

	@PutMapping("/products/{id}")
	Product updateProduct(@RequestBody Product newProduct, @PathVariable Long id) {

		return repository.findById(id).map(product -> {
			product.setCode(newProduct.getCode());
			product.setName(newProduct.getName());
			product.setDescription(newProduct.getDescription());
			product.setPrice(newProduct.getPrice());
			return repository.save(product);
		}).orElseGet(() -> {
			newProduct.setId(id);
			return repository.save(newProduct);
		});
	}

// Supprimer un produit

	@DeleteMapping("/products/{id}")
	void deleteProduct(@PathVariable Long id) {
		repository.deleteById(id);
	}
}
