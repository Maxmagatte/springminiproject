package com.finappli.SptingProject.controller;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.finappli.SptingProject.dao.PanierRepository;
import com.finappli.SptingProject.models.Panier;
import com.finappli.SptingProject.models.Product;
import com.finappli.SptingProject.utils.ProductNotFoundException;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "http://localhost:8081")
public class PanierController {

  private final PanierRepository repository;

  PanierController(PanierRepository repository) {
    this.repository = repository;
  }

// Recuperer la liste des paniers
  @GetMapping("/paniers")
  List<Panier> all() {
    return repository.findAll();
  }

  // Afficher  les informations d'un panier
  @GetMapping("/paniers/{id}")
	Panier findOne(@PathVariable Long id) {

		return repository.findById(id)
				.orElseThrow(() -> new ProductNotFoundException(id));
	}
  
// ajouter un produit dans un panier
  @PostMapping("/paniers/{id}/product")
  Panier addProduct(@RequestBody Panier newPanier, @PathVariable Long id , Product product) {

    return repository.findById(id)
      .map(panier -> {
      List<Product> listProduct = panier.getProducts();
       listProduct.add(product);
        return repository.save(panier);
      })
      .orElseGet(() -> {
    	  newPanier.setId(id);
        return repository.save(newPanier);
      });
  }
// Enlever un produit du panier
  
  @PutMapping("/paniers/{id}")
  Panier removeOneProduct(@RequestBody Panier newPanier, @PathVariable Long id , Product product) {
    return repository.findById(id)
      .map(panier -> {
      List<Product> listProduct = panier.getProducts();
      for(Product p : listProduct) {
    	  p = product;
          listProduct.remove(product);
      }
        return repository.save(panier);
      })
      .orElseGet(() -> {
    	  newPanier.setId(id);
        return repository.save(newPanier);
      });
  }
  // Aficher le contenu d un panier( liste de produit)
  
  @GetMapping("/paniers/{id}")
  List<Product> displayBoxContain(@PathVariable Long idpanier ) {
	  List<Product> listProduct = new ArrayList<Product>();
    return repository.findById(idpanier)
     .map( panier -> {
      listProduct = panier.getProducts();
      //return repository.save(panier);
      });
    return  listProduct;
  }
 

   
  
}