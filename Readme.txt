# Guide démarrage du projet

Ce projet est un mini api fait avec springboot.Il permet de creer,modifier ,supprimer,... un produit


Pour démarrer le projet il faut :

0. s assurant qu on a java installé sur son ordinateur


1.Recuperer le projet sur le lien  gitlab suivant: 

 git clone  git@gitlab.com:Maxmagatte/springminiproject.git
 
2.Ouvirir le projet sur un terminal en tapant :


cd + le chemin du projet dans votre ordinateur 


3. Lancer le projet en tapant la commande : 


         ./mvnw spring-boot:run
         
         
4. Tester l api en tapant les commandes suivantes sur un terminal



 * Afficher le catalogue de produits :
 
 
  curl http://localhost:8080/api/products
 
 
 * Afficher les informations d 'un produit :
 
 

   curl http://localhost:8080/api/products/identifiant produit
   
   
 * Modifier les informations d'un produit : 
 
 
  curl -X DELETE http://localhost:8080/api/products/identifiant 
  
 * Ajouter un produit :  
 
      
   curl http://localhost:8080/api/products
   
   * Ajouter un nouveau panier
   
   http://localhost:8081/api/paniers
   
   * Ajouter un produit a un panier
   
   http://localhost:8081/api/paniers/id      ( params : idpanier  , product) 
   
   * Enlever un produit du panier
   
      http://localhost:8081/api/paniers/id      ( params : idpanier  , product) 
      
   * Afficher le contenu d un panier
   
      http://localhost:8081/api/paniers/id      ( params : idpanier  ) 
   
   